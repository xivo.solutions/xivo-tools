# Automatic queue creation script

Since there is no API to create queues a browser automation script was developed to easily add more queues into any XiVO platform.  
It uses puppeteer to automate a browser in order to create the queues.

# Usage of script

This script is ran from a docker container to aviod installing all the NodeJS dependencies on the host.  
To configure the script (how many queues to create, what extensions to use and more) please copy the env_template like so:  

```
cp env_template .env
```

Fill up the .env file. Here is an example:  

```
WEBI_LOGIN='webiLogin'
WEBI_PWD='webiPass'
XIVO_IP='192.168.1.1'
QUEUE_PREFIX='my_queue_'
QUEUE_PREFIX_START_NUMBER=100
QUEUE_NUMBER=200
CONTEXT='default'
QUEUE_COUNT=50
PUPPETEER_SKIP_CHROMIUM_INSTALL=false
CHROME_EXECUTABLE='/usr/bin/chromium-browser'
```

*Note: QUEUE_PREFIX_START_NUMBER is meant to be an index, that is appended to QUEUE_PREFIX*  

And lastly run:  

```
docker-compose up
```

