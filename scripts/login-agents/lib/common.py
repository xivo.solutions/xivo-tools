#!/usr/bin/python3

import requests

def api_call(method, url, headers = None, payload = None, auth = None):
    try:
        response = requests.request(method, url, headers=headers, data=payload, auth=auth, verify=False)
        response.raise_for_status()
    except requests.exceptions.HTTPError as HTTPError:
        print(f"HTTP Error: {HTTPError}")
    except requests.exceptions.ConnectionError:
        print(f"Can't connect to {url}")
    except requests.exceptions.Timeout:
        print(f"Timeout of request to {url}")
    except requests.exceptions.RequestException as ReqError:
        print(f"RequestException error has occured: {ReqError}")

    if method!="GET":
        print(response.text)
    return response