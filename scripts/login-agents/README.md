# Automate agent login

This script can do several agent actions. The allowed agent actions are:  

```
login
logoff
logoff all
relog
```

To apply the action just use it as argument:  

```bash
python agents_actions.py <action> <first_exten> <count> <context>
```  

# Starting with python script

- Copy the config_file_template in a file named config_file.py
- Fill the variables
- Start the script with ```python3 agents_login.py```