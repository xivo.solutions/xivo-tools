These scripts improve some scripts in "edit_users" directory.

Purpose is to create users and agents with human names and change `host` variable to SIPp loadtester.

## Usage

Create csv lists `male.csv`, `female.csv` and `surnames.csv` with column `name`.

You can find list of names and surnames e.g. at:

- https://www.behindthename.com/top/lists/france/2016
- https://surnames.behindthename.com/top/lists/france/2005

Edit variables at top of script `generate_user_csv.py`.

Run the script.

If the output is ok, run it again with output redirection to file.

Import the csv to XiVO web interface.

Edit variables at top of script `generate_agent_sql.py`.

Run `python generate_agent_sql.py` with the generated csv file name as argument.

Execute the output sql commands in asterisk database. To execute a file you can use
command `sudo -u postgres psql -d asterisk -a -f /var/tmp/commands.sql`

The `SELECT` commands are for verification of the actions.

Run `sudo -u postgres psql asterisk -c "SELECT id FROM agentfeatures WHERE number BETWEEN '2350' AND '2399'"` and copy
the list.

If the list is long, you can redirect the console output to file and replace line breaks in the file
with `tr "\n" ", " < filename`

Run python CLI and use IDs of the new agents to generate list of INSERT commands:

    all=(200, 201, 202, 203, 205, 206, 207, 208, 209, 210)
    sql = ''
    for id in all:
        sql += "INSERT INTO contextmember VALUES ('loadtest', 'agent', " + str(id) + ", 'context');\n"

    f = open("contextmember", "w")
    f.write(sql)
    f.close()

Execute these sql commands in asterisk database: `sudo -u postgres psql -d asterisk -a -f /var/tmp/contextmember.sql`

Restart XiVO and XUC.
