import csv
import random

list_length = 50
exten = 2350
names_is_csv = 499
surnames_is_csv = 999


def read_csv(filename):
    csv_data = []
    with open(filename) as csvfile:
        csv_file = csv.DictReader(csvfile)
        for row in csv_file:
            csv_data.append(row)

    return csv_data


male = read_csv('male.csv')
female = read_csv('female.csv')
surnames = read_csv('surnames.csv')

print(
    "entity_id,firstname,lastname,username,password,cti_profile_name,cti_profile_enabled,context,line_protocol,exten,sip_username,sip_secret")

for name in range(list_length):
    name_index = int(random.random() * names_is_csv)
    surname_index = int(random.random() * surnames_is_csv)

    if random.random() > 0.5:
        firstname = male[name_index]['name']
    else:
        firstname = female[name_index]['name']

    lastname = surnames[surname_index]['name']

    random_number = random.random()

    if random_number > 0.66:
        profile = "Agent"
    elif random_number > 0.33:
        profile = "Client"
    else:
        profile = "Supervisor"

    print("1," + firstname + "," + lastname + "," + str(exten) + ",0000," + profile + ",1,loadtest,sip," + str(
        exten) + "," + str(exten) + ",0000")

    exten += 1
