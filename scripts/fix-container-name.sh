#!/bin/bash

# Standard color
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NOCOLOR='\033[0m'

ERROR=0

function usage {
    echo "Usage: fix-container-name.sh <old_project_name> <new_project_name>"
    exit -1
}

function inc_error {
    ERROR=$(($ERROR+1))
}

function check_container_state {
    CONTAINER=$1
    echo -ne " * ${CONTAINER}... "
    CONTAINER_STATE=$(docker inspect --format '{{.State.Status}}' ${CONTAINER} 2>/dev/null)
    if [ -z "$CONTAINER_STATE" ]; then
        inc_error
        echo -e "${RED}container not found${NOCOLOR}"
    elif [ "$CONTAINER_STATE" != "exited" ]; then
        inc_error
        echo -e "${RED}invalid state: '${CONTAINER_STATE}', expecting to be exited${NOCOLOR}"
    else
        echo -e "${GREEN}Ok${NOCOLOR}"
    fi
}

function docker_inspect {
    FORMAT=$1
    CONTAINER=$2
    
    echo -ne " * ${CONTAINER}... "
    INSPECT_RESULT=$(docker inspect --format="${FORMAT}" ${CONTAINER} 2>/dev/null)

    if [ -z $INSPECT_RESULT ]; then
        echo -e "${RED}not available${NOCOLOR}"
        exit -1
    else
        echo -e "${GREEN}Ok${NOCOLOR}"
    fi
}

if [ -z $1 ]; then
    echo "Required argument missing: old project name"
    usage
fi

if [ -z $2 ]; then
    echo "Required argument missing: new project name"
    usage
fi

OLD_PROJECT=$1
NEW_PROJECT=$2

CONTAINERS=(_postgresvols_1 _elasticsearch_1)
PREFIXES=($OLD_PROJECT $NEW_PROJECT)


echo "Checking containers states:"
for prefix in "${PREFIXES[@]}"
do
    for postfix in "${CONTAINERS[@]}"
    do
        check_container_state "${prefix}${postfix}"        
    done
done

if [ $ERROR -gt 0 ]; then
    echo "Errors, aborting"
    exit -1
fi

echo -e "\nChecking volumes availibility:"

PG_INSPECT_FORMAT='{{(index .Mounts 0).Name}}'
ES_INSPECT_FORMAT='{{range $i, $value := .Mounts}}{{if eq $value.Destination "/usr/share/elasticsearch/data"}}{{$value.Name}}{{end}}{{end}}'

docker_inspect "$PG_INSPECT_FORMAT" "${OLD_PROJECT}_postgresvols_1"
OLD_PGVOL=$INSPECT_RESULT

docker_inspect "$PG_INSPECT_FORMAT" "${NEW_PROJECT}_postgresvols_1"
NEW_PGVOL=$INSPECT_RESULT

docker_inspect "$ES_INSPECT_FORMAT" "${OLD_PROJECT}_elasticsearch_1"
OLD_ESVOL=$INSPECT_RESULT

docker_inspect "$ES_INSPECT_FORMAT" "${NEW_PROJECT}_elasticsearch_1"
NEW_ESVOL=$INSPECT_RESULT

# Checking Needed space
SIZE_PG=$(docker run --rm -it -v ${OLD_PGVOL}:/tmp/oldpg ubuntu:14.04.3 /bin/bash -c 'du -hs /tmp/oldpg | cut -f1')
SIZE_ES=$(docker run --rm -it -v ${OLD_ESVOL}:/tmp/oldes ubuntu:14.04.3 /bin/bash -c 'du -hs /tmp/oldes | cut -f1')

echo -e "\nYou are about to copy data from $OLD_PROJECT to $NEW_PROJECT, ${YELLOW}This action cannot be undone !${NOCOLOR}"
echo "Also, ensure you have the required space before continuing:"
echo " * Required space for postgres data: ${SIZE_PG}"
echo " * Required space for elastic data: ${SIZE_ES}"
read -p "Are you sure you want to continue? (y/N): " -n 1 -r

if [[ ! $REPLY =~ ^[Yy]$ ]]; then
    echo -e "\nAborting"
    exit -1
fi


echo -en "\n\nMigrating postgres data: "
docker run --rm -it -v ${OLD_PGVOL}:/tmp/oldpg -v ${NEW_PGVOL}:/tmp/newpg ubuntu:14.04.3 /bin/bash -c 'cp -aR /tmp/oldpg/* /tmp/newpg'
echo -e "${GREEN}done${NOCOLOR}"

echo -en "Migrating elasticsearch data: "
docker run --rm -it -v ${OLD_ESVOL}:/tmp/oldes -v ${NEW_ESVOL}:/tmp/newes ubuntu:14.04.3 /bin/bash -c 'cp -aR /tmp/oldes/* /tmp/newes'
echo -e "${GREEN}done${NOCOLOR}"
