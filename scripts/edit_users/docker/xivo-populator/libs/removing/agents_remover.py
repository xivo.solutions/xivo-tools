#!/usr/bin/python3

import json
from libs.common import api_call, HEADERS
from libs.config import CONFIG

def try_get_queue_id(agent_id):
    print(f"Fetching all queues of agent {agent_id}")
    url = f"{CONFIG['url_prefix']}/agents/{agent_id}/queues"
    response = api_call("GET", url, HEADERS)
    queues = json.loads(response.text)
    if (len(queues) == 0):
        return None
    else:
        queue_id = queues[0]['queue_id']
        return queue_id
    

def delete_agent_from_queue(agent_id, queue_id):
    print(f"Removing agent {agent_id} from queue {queue_id}")
    url = f"{CONFIG['url_prefix']}/queues/{queue_id}/members/agents/{agent_id}"
    api_call("DELETE", url, HEADERS)


def delete_agent(agent_id):
    print(f"Deleting agent {agent_id}")
    url = f"{CONFIG['url_prefix']}/agents/{agent_id}"
    api_call("DELETE", url, HEADERS)