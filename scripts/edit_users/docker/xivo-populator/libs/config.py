#!/usr/bin/python3

import os

CONFIG = {

    "create_users": os.environ.get('create_users'),
    "create_agents": os.environ.get('create_agents'),
    "populate_queue": os.environ.get('populate_queue'),
    "ws_user": os.environ.get('ws_user'),
    "ws_pass": os.environ.get('ws_pass'),
    "url_prefix": f"https://{os.environ.get('xivo_ip')}:9486/1.1",
    "user_tag": os.environ.get('user_tag'),
    "line_type": os.environ.get('line_type'),
    "first_exten": int(os.environ.get('first_exten')),
    "password": os.environ.get('password'),
    "count": int(os.environ.get('count')),
    "context": os.environ.get('context'),
    "mds": os.environ.get('mds'),
    "user_tag_increment": os.environ.get('user_tag_increment'),
    "user_tag_first": os.environ.get('user_tag_first'),
    "agents_group": int(os.environ.get('agents_group')),
    "queue_id": int(os.environ.get('queue_id')),
    "delete_everything" : os.environ.get('delete_everything'),

    "plugin_config": {
        "Yealink": {
            "xivo-yealink-v85": [
                "T31P",
                "T33G",
                "T53",
                "T54W",
                "T57W",
                "W73P"
            ]
        },
        "Snom": {
            "xivo-snom-10.1.46.16": [
                "D712",
                "715",
                "D715",
                "D717",
                "D725",
                "D735",
                "D765",
                "D785"
            ]
        }
    },
    "auth": "",
    "line_ids": [],
    "device_ids": []
}