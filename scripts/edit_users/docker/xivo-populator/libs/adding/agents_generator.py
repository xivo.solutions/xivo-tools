# /usr/bin/python

import json
from libs.common import api_call, HEADERS
from libs.config import CONFIG


def create_agents():
    print("Creating agents..")
    method = "POST"
    url = f"{CONFIG['url_prefix']}/agents"

    for current_exten in range(CONFIG["first_exten"], CONFIG["first_exten"] + CONFIG["count"]):
        data = {
            "firstname": f"{current_exten}",
            "lastname": f"{current_exten}",
            "number": f"{current_exten}",
            "numgroup": f"{CONFIG['agents_group']}",
            "context": f"{CONFIG['context']}"
        }
        payload = json.dumps(data)
        api_call(method, url, HEADERS, payload)
    first_user_uuid=get_first_user_uuid()
    first_user_id=get_first_user_id(first_user_uuid)
    first_agent_id=get_first_agent_id()
    update_users_with_agents(first_user_id, first_agent_id)


def get_first_user_uuid():
    method = "GET"
    url = f"{CONFIG['url_prefix']}/users/export"

    users_list = json.loads(api_call(method, url, HEADERS).text)
    matched_uuids = [item['uuid'] for item in users_list['content'] if item.get('exten') == str(CONFIG["first_exten"])]
    if matched_uuids:
        first_user_uuid = matched_uuids[0]
        return first_user_uuid
    else:
        print(f"No user with 'number' {CONFIG['first_exten']} found.")
        exit()


def get_first_user_id(first_user_uuid):
    method = "GET"
    url = f"{CONFIG['url_prefix']}/users"

    users_list = json.loads(api_call(method, url, HEADERS).text)
    matched_ids = [item['id'] for item in users_list['items'] if item['uuid'] == str(first_user_uuid)]
    if matched_ids:
        first_user_id = matched_ids[0]
        return first_user_id
    else:
        print(f"No user with 'number' {CONFIG['first_exten']} found.")
        exit()


def get_first_agent_id():
    method = "GET"
    url = f"{CONFIG['url_prefix']}/agents"

    agents_list = json.loads(api_call(method, url, HEADERS).text)
    matched_ids = [item['id'] for item in agents_list['items'] if item['number'] == str(CONFIG["first_exten"])]
    if matched_ids:
        first_agent_id = matched_ids[0]
        return first_agent_id
    else:
        print(f"No agent with 'number' {CONFIG['first_exten']} found.")
        exit()


def update_users_with_agents(first_user_id, first_agent_id):
    method = "PUT"

    current_agent_id = first_agent_id
    for user_id in range(first_user_id, first_user_id + CONFIG["count"]):
        url = f"{CONFIG['url_prefix']}/users/{user_id}"
        data = {
            "id": user_id,
            "agentid": current_agent_id
        }
        current_agent_id += 1
        payload = json.dumps(data)
        api_call(method, url, HEADERS, payload)


def queue_populate():
    first_agent_id=get_first_agent_id()
    print("Populating the queue with agents...")
    method="POST"
    url = f"{CONFIG['url_prefix']}/queues/{CONFIG['queue_id']}/members/agents"
    for agent_id in range (first_agent_id, first_agent_id+CONFIG["count"]):
        data= {
            "agent_id": agent_id,
            "queue_id": CONFIG['queue_id'],
            "penalty": 0
        }
        payload = json.dumps(data)
        api_call(method, url, HEADERS, payload)