#!/usr/bin/python3

import io
import csv
import sys


def create_users(user_tag, line_type, first_exten, password, count, context, mds, user_tag_increment, user_tag_first):
	users = []
	for increment in range(count):
		exten = first_exten + increment
		firstname = user_tag
		lastname = exten
		if user_tag_increment:
			name = user_tag + str(int(user_tag_first) + increment).zfill(len(user_tag_first))
			firstname = name
			lastname = name
		users.append({
			'entity_id':					1,
			'firstname':					firstname,
			'lastname':						lastname,
			'outgoing_caller_id':			'default',
			'language':						'fr_FR',
			'enabled':						1,
			'ring_seconds':					30,
			'simultaneous_calls':			5,
			'supervision_enabled':			1,
			'call_transfer_enabled':		1,
			'username':						exten,
			'password':						password,
			'cti_profile_name':				'Agent',
			'cti_profile_enabled':			1,
			'line_protocol':				line_type,
			'line_site':					mds,
			'context':						context,
			'exten':						exten,
		})

	output = io.StringIO()
	field_names = users[0].keys()
	writer = csv.DictWriter(output, fieldnames=field_names)
	writer.writeheader()
	writer.writerows(users)
	return output.getvalue()


def main():
	first_exten = int(sys.argv[1])
	count = int(sys.argv[2])
	context = sys.argv[3]
	mds = sys.argv[4]
	create_users(first_exten, count, context, mds)

if __name__ == "__main__":
	main()