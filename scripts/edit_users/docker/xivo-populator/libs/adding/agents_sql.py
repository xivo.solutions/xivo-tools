#!/usr/bin/python3
import sys

first_user = 4000
add_users = 2000
context = "loadtest"
agents_group_id = str(2)
filename = sys.argv[1]

with open("%s" % (filename), 'w') as sqlfile:
    for user in range(add_users):
        exten = str(first_user + user)
        sqlfile.write("INSERT INTO agentfeatures "
                      "VALUES "
                      "(default,"
                      + agents_group_id + ","
                      "'tester',"
                      + exten + ","
                      + exten + ","
                      "'',"
                      "'" + context + "',"
                      "'',"
                      "0,"
                      "'',"
                      "'',"
                      "'');"
                      "\n")

    sqlfile.write("\n"
                  "UPDATE userfeatures \n"
                  "SET agentid = agentfeatures.id \n"
                  "FROM agentfeatures \n"
                  "WHERE agentfeatures.number = userfeatures.lastname \n"
                  "AND userfeatures.firstname = 'tester';")
