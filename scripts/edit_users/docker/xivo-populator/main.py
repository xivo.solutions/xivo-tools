#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import sys
sys.path.append('libs')

from libs.generator import add_config
from libs.remover import remove_config
from libs.config import CONFIG


def main(): 
    if CONFIG["delete_everything"].lower() == "true":
        remove_config()
    else:
        add_config()


if __name__ == "__main__":
    main()

